

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

//push using navigatorKey
void changeScreenPush(BuildContext context,Widget widget){
  //  navigatorKey.currentState
  //             .push(MaterialPageRoute(builder: (_) => widget));
   Navigator.push(context, MaterialPageRoute(builder: (_) => widget));
}

void changeScreenReplacement(BuildContext context,Widget widget){
  // navigatorKey.currentState
  //             .pushReplacement(MaterialPageRoute(builder: (_) => widget));
               Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => widget));
}

void changeScreenRemoveUntil(BuildContext context,Widget widget){
  Navigator.pushAndRemoveUntil(context,MaterialPageRoute(builder: (_) => widget),(Route<dynamic> route) => false,);
}

void changeScreenReplacementPage(BuildContext context, Widget widget){
  Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => widget));
}