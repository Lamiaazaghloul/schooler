import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'const_color.dart';

class AppTheme{

  AppTheme._();

  static const Color appBackgroundColor=PrimaryDarkColor;
  static const Color topBarBackgroundColor=PrimaryColor;
  static const Color selectedTabBackgroundColor=PrimaryDarkColor;
  static const Color UnSelectedBackgroundColor=Color(0xFFFFFFfC);
  static const Color subTitleTexColor=Color(0xFF9F988F);

  static final ThemeData lightTheme=ThemeData(
  //  scaffoldBackgroundColor: AppTheme.appBackgroundColor,
    brightness: Brightness.light,
    textTheme: lightTextTheme,
    fontFamily: 'cairo',
    primaryColor: PrimaryLightColor,
    accentColor: AccentColor,
    primaryColorDark: PrimaryDarkColor,
    primaryColorLight: PrimaryLightColor,
    //  canvasColor: Colors.transparent,
    
  );
  static final ThemeData darkTheme=ThemeData(
      scaffoldBackgroundColor: Colors.black,
      brightness: Brightness.dark,
      textTheme: darkTextTheme
  );


  static final TextTheme lightTextTheme=TextTheme(
    // title: lightTextStyle,
    // subtitle:lightSubTitleStyle ,
    button: _lightButtonStyle,
  );

  static final TextTheme darkTextTheme=TextTheme(
    // title: darkTextStyle,
    // subtitle: darkSubTitleStyle,
    button: _darkButtonStyle,
  );

  static final TextStyle lightTextStyle=TextStyle(
    color: Colors.black,
    fontSize: 28
  );

  static final TextStyle lightSubTitleStyle=TextStyle(
      color: subTitleTexColor,
      fontSize: 18,
      height: 1.5
  );
  static final TextStyle _lightButtonStyle=TextStyle(
      color: Colors.black,
      fontSize: 18
  );


  static final TextStyle darkTextStyle=TextStyle(
      color: Colors.white,
      fontSize: 18
  );

  static final TextStyle darkSubTitleStyle=TextStyle(
      color: Colors.white70,
      fontSize: 18,
      height: 1.5
  );
  static final TextStyle _darkButtonStyle=TextStyle(
     // color: Colors.white,
      fontSize: 18
  );
  


}