import 'package:flutter/material.dart';

const PrimaryColor = Color(0xFFFF2D00);
const PrimaryLightColor = Color(0xFFFFFFFF);
const PrimaryDarkColor = Color(0xFF000000);
const AccentColor = Color(0xFFF2F2F2);
const GreyColor= Color(0xFFeeeeee);
const WhiteColor=Color(0xFFFFFFFF);

const LableColor = Color(0xFF6C6C6C);
const ErrorColor = Color(0xFF888888);
const BackGroundColor=Color(0xFFeeeeee);

const double TitleFontSize=14;
const double SubTitleFontSize=12;

