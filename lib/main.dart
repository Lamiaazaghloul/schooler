import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:schooler/provider/connectivity_provider.dart';
import 'package:schooler/provider/home_provider.dart';
import 'package:schooler/provider/translations.dart';
import 'package:schooler/ui/splash.dart';
import 'package:schooler/uitils/app_theme.dart';
import 'package:schooler/uitils/size_config.dart';
import 'enums/connectivity_status.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamProvider<ConnectivityStatus>(
        create: (_) => ConnectivityService().connectionStatusController.stream,
        child: MultiProvider(
          providers: [
            ChangeNotifierProvider<HomeProvider>(create: (_) => HomeProvider()),
            // ChangeNotifierProvider<MealsDetailsProvider>(
            //     create: (_) => MealsDetailsProvider()),
            // ChangeNotifierProvider<MealsProvider>(
            //     create: (_) => MealsProvider()),
          ],
          child: LayoutBuilder(
            builder: (context, constraints) {
              return OrientationBuilder(
                builder: (context, orientation) {
                  SizeConfig().init(constraints, orientation);
                  return MaterialApp(
                    debugShowCheckedModeBanner: false,
                    theme: AppTheme.lightTheme,
                    home: SplashPage(),

                    localizationsDelegates: [
                      const TranslationsDelegate(),
                      GlobalMaterialLocalizations.delegate,
                      GlobalWidgetsLocalizations.delegate,
                      GlobalCupertinoLocalizations.delegate,
                    ],
                    supportedLocales: [
                    Locale('ar', 'AE') 
                     ],
                    locale: const Locale('ar'),
                  );
                },
              );
            },
          ),
        ));
  }
}
