import 'package:flutter/material.dart';
import 'package:schooler/ui/widget/layoutWidget.dart';
import 'package:table_calendar/table_calendar.dart';

class AcademicCalendarPage extends StatefulWidget {
  @override
  _AcademicCalendarPageState createState() => _AcademicCalendarPageState();
}

class _AcademicCalendarPageState extends State<AcademicCalendarPage> {
CalendarController _calendarController;
@override
void initState() {
  super.initState();
  _calendarController = CalendarController();
}

@override
void dispose() {
  _calendarController.dispose();
  super.dispose();
}

@override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWidget(title: " الخطة الاسبوعية    ", context: context),
      body: Column(children: [
        Container(
          height: MediaQuery.of(context).size.height/1.5,
          child:TableCalendar(
        locale: 'ar',
    calendarController: _calendarController,
  )),
  Expanded(child: Text("لم تتم اضافة احداث"))
  
  ],));

  }
}
