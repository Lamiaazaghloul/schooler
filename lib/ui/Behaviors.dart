import 'package:flutter/material.dart';
import 'package:schooler/ui/widget/layoutWidget.dart';
import 'package:schooler/uitils/const_color.dart';

class Behaviors extends StatefulWidget {
  @override
  _BehaviorsState createState() => _BehaviorsState();
}
 
class _BehaviorsState extends State<Behaviors> {
    List tabs=[' الكل','ايجابي','سلبي'];
     int _selectedIndex = 0;
    _onSelected(int index) {
        setState(() => _selectedIndex = index);
        print(index);
    }
  @override
  Widget build(BuildContext context) {
     return Scaffold(
      appBar: appBarWidget(title: " السلوكيات ", context: context),
      body:Column(children: [
        
        Container(
          margin: EdgeInsets.symmetric(vertical: 20.0),
          height: 40.0,
          child: ListView.builder(
             scrollDirection: Axis.horizontal,
             shrinkWrap: false,
      itemCount: tabs.length,
      itemBuilder: (context, index) {
 return InkWell(
     onTap: () => _onSelected(index),
     child:Container(
     decoration: BoxDecoration(
     color: _selectedIndex != null && _selectedIndex == index
                ? PrimaryColor
                : ErrorColor,
          borderRadius: BorderRadius.circular(50),
     ),
     padding: EdgeInsets.only(left: 15,right: 15,top: 4),
       margin: EdgeInsets.only(left: 5,right: 5),
     child:Text(tabs[index],style: TextStyle(color: AccentColor),textAlign: TextAlign.center,),
  ));
            }
)),
        Expanded(child:
        
    Center(
      child:Text("لم يتم العثور علي محتوي")
      //     child: ListView.builder(
      //   itemCount: 8,
      //   itemBuilder: (context, index) {
      //     return _selectedIndex != null && _selectedIndex == index?examAssignmentsWidget(
      //         title: "الاسلوب اللغوي",
             
      //         ):assignmentsWidget(
      //         title: "الخصائص والتغيرات الكيميائية",
             
      //         );
      //   },
      // )
      ))]),
    );
  }
}
