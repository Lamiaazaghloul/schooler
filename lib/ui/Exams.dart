import 'package:flutter/material.dart';
import 'package:schooler/ui/widget/layoutWidget.dart';
import 'package:schooler/ui/widget/listAppwidget.dart';


class ExamsPage extends StatefulWidget {
  @override
  _ExamsPageState createState() => _ExamsPageState();
}

class _ExamsPageState extends State<ExamsPage> {
  @override
  Widget build(BuildContext context) {
     return Scaffold(
      appBar: appBarWidget(title: "الامتحانات ", context: context),
      body: Container(
          child: ListView.builder(
        itemCount: 2,
        itemBuilder: (context, index) {
          return examWidget(
              title: "مدرس",
              subtitle: "رياض حسام ",
              lableValue: "امتحان ( ١ ) ");
        },
      )),
    );
  }
}
