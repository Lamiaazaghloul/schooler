
import 'package:flutter/material.dart';
import 'package:schooler/ui/register.dart';
import 'package:schooler/ui/sidemenu.dart';
import 'package:schooler/ui/widget/widgetRaisedButton.dart';
import 'package:schooler/ui/widget/widgetTextField.dart';
import 'package:schooler/uitils/change_route.dart';
import 'package:schooler/uitils/const_color.dart';

import 'home.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {

  TextEditingController userName=new TextEditingController();
  TextEditingController password=new TextEditingController();
  double radiusValue=30;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: DrawerPage(),
        backgroundColor: PrimaryLightColor,
        body: Container(
            padding: EdgeInsets.all(20),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    color: PrimaryLightColor,
                    child: Image.asset(
                      'assets/logo.png',
                      height: 150,
                      width: 260,
                    )
                    // ),
                    ),

                  Container(
                    color: PrimaryLightColor,
                    child:widgetTextField(
                      hintText: 'اسم المستخدم',
                      labelText: 'اسم المستخدم',
                      controller: userName,
                      textInput: TextInputType.name,
                      radius: radiusValue

                    )
                  ),
SizedBox(height:15),
                  Container(
                    color: PrimaryLightColor,
                    child:widgetTextField(
                      hintText: 'كلمة المرور ',
                      labelText: 'كلمة المرور',
                      controller: password,
                      textInput: TextInputType.name,
                      radius: radiusValue

                    )
                  ),
SizedBox(height:25),
                    Container(
                  width: 170,
                  height: 50,
                    child: raisedButton(
                        buttonColor: PrimaryDarkColor,
                        ontap: () {
                          changeScreenRemoveUntil(context,HomePage());
                        },
                        labelText: 'تسجيل الدخول')),

                
                
                ])
                
                ),
                 bottomNavigationBar: InkWell(
                   onTap: (){
                     changeScreenPush(context,Register());
                   },
                   child: new Container(
                   margin: EdgeInsets.only(bottom: 25,left: 100,right: 100),

    height: 50.0,
    width: 60.0,
    decoration: BoxDecoration(
      color:PrimaryLightColor,
      border: Border.all(color: ErrorColor),
      borderRadius: BorderRadius.circular(25),
    ),
    child:Center(child:Text('التقدم في المدرسة'))
  ),
                ));
  }
}
