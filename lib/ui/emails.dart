import 'package:flutter/material.dart';
import 'package:schooler/ui/widget/layoutWidget.dart';
import 'package:schooler/ui/widget/listAppwidget.dart';
import 'package:schooler/uitils/const_color.dart';


class EmailsPage extends StatefulWidget {
  @override
  _EmailsPageState createState() => _EmailsPageState();
}

class _EmailsPageState extends State<EmailsPage> {
  List tabs=['البريد الوارد','الصادر','المحذوف'];
  @override
  Widget build(BuildContext context) {
     return Scaffold(
      appBar: appBarWidget(title: "البريد ", context: context),
      body:Column(children: [
        
        Container(
          margin: EdgeInsets.symmetric(vertical: 20.0),
          height: 40.0,
          child: ListView.builder(
             scrollDirection: Axis.horizontal,
             shrinkWrap: false,
      itemCount: tabs.length,
      itemBuilder: (context, index) {
 return InkWell(
     onTap: (){},
     child:Container(
     decoration: BoxDecoration(
       color: PrimaryColor,
          borderRadius: BorderRadius.circular(50),
     ),
     padding: EdgeInsets.only(left: 15,right: 15,top: 4),
       margin: EdgeInsets.only(left: 5,right: 5),
     child:Text(tabs[index],style: TextStyle(color: AccentColor),textAlign: TextAlign.center,),
  ));
            }
)),
        Expanded(child:
    Container(
          child: ListView.builder(
        itemCount: 8,
        itemBuilder: (context, index) {
          return emailsWidget(
              title: "مصطفي احمد اسماعيل",
              subtitle: "شكرا يا ابني وبالتوفيق  ",
              );
        },
      )))]),
    );
  }
}
