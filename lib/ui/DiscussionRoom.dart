import 'package:flutter/material.dart';
import 'package:schooler/ui/widget/layoutWidget.dart';
import 'package:schooler/uitils/const_color.dart';

class DiscussionRoomPage extends StatefulWidget {
  @override
  _DiscussionRoomPageState createState() => _DiscussionRoomPageState();
}

class _DiscussionRoomPageState extends State<DiscussionRoomPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWidget(title: "غرف النقاش", context: context),
      body:Container(
        child:GridView.count(
  primary: false,
  padding: const EdgeInsets.all(5),
  crossAxisSpacing: 10,
  mainAxisSpacing: 10,
  crossAxisCount: 2,
  children: <Widget>[
    Card(
      elevation: 1,
      child: Column(children: [
        Image.network('https://banner.uclipart.com/20200112/roc/teacher-circle-line.png',width: 70,height:70,),
        Text('مصطفي احمد',style: TextStyle(fontWeight: FontWeight.bold),),
        Text('العنوان : الاعداد والصحة'),
        Text('القيمة المطلقة'),
        Text('قبل ١٤ ساعه'),

      ],),
      color:WhiteColor,
    ),
    Card(
      elevation: 1,
      child: Column(children: [
       Image.network('https://banner.uclipart.com/20200112/roc/teacher-circle-line.png',width: 70,height:70,),
               Text('مصطفي احمد',style: TextStyle(fontWeight: FontWeight.bold),),

        Text('العنوان : الاعداد والصحة'),
        Text('القيمة المطلقة'),
        Text('قبل ١٤ ساعه'),

      ],),
      color:WhiteColor,
    ),
     Card(
      elevation: 1,
      child: Column(children: [
        Image.network('https://banner.uclipart.com/20200112/roc/teacher-circle-line.png',width: 70,height:70,),
               Text('مصطفي احمد',style: TextStyle(fontWeight: FontWeight.bold),),

        Text('العنوان : الاعداد والصحة'),
        Text('القيمة المطلقة'),
        Text('قبل ١٤ ساعه'),

      ],),
      color:WhiteColor,
    ),
     Card(
      elevation: 1,
      child: Column(children: [
        Image.network('https://banner.uclipart.com/20200112/roc/teacher-circle-line.png',width: 70,height:70,),
               Text('مصطفي احمد',style: TextStyle(fontWeight: FontWeight.bold),),

        Text('العنوان : الاعداد والصحة'),
        Text('القيمة المطلقة'),
        Text('قبل ١٤ ساعه'),

      ],),
      color:WhiteColor,
    ),
    
  ],
)));
  
  
  }}