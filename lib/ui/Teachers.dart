import 'package:flutter/material.dart';
import 'package:schooler/ui/widget/layoutWidget.dart';
import 'package:schooler/ui/widget/listAppwidget.dart';


class TeachersPage extends StatefulWidget {
  @override
  _TeachersPageState createState() => _TeachersPageState();
}

class _TeachersPageState extends State<TeachersPage> {
  @override
  Widget build(BuildContext context) {
     return Scaffold(
      appBar: appBarWidget(title: "المدرسين", context: context),
      body: Container(
          child: ListView.builder(
        itemCount: 2,
        itemBuilder: (context, index) {
          return teacherWidget(
              title: "محمد مصطفي",
              subtitle: "مدرب معتمد  ",
              lableValue: "عرض المزيد ");
        },
      )),
    );
  }
}
