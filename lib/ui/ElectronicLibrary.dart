import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:schooler/ui/widget/layoutWidget.dart';
import 'package:schooler/uitils/const_color.dart';


class ElectronicLibraryPage extends StatefulWidget {
  @override
  _ElectronicLibraryPageState createState() => _ElectronicLibraryPageState();
}

class _ElectronicLibraryPageState extends State<ElectronicLibraryPage> {


bool isPressed = false;
List tabs=['الكل','المواد','مشاهده','برامج','فلاش ','اصوات'];

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: appBarWidget(title: "المكتبة الالكترونية ", context: context),
      body: Column(children: [
        
        Container(
          margin: EdgeInsets.symmetric(vertical: 20.0),
          height: 40.0,
          child: ListView.builder(
             scrollDirection: Axis.horizontal,
             shrinkWrap: false,
      itemCount: tabs.length,
      itemBuilder: (context, index) {
 return InkWell(
     onTap: (){},
     child:Container(
     decoration: BoxDecoration(
       color: PrimaryColor,
          borderRadius: BorderRadius.circular(50),
     ),
     padding: EdgeInsets.only(left: 15,right: 15,top: 4),
       margin: EdgeInsets.only(left: 5,right: 5),
     child:Text(tabs[index],style: TextStyle(color: AccentColor),textAlign: TextAlign.center,),
  ));
            }
)),
        Expanded(child:
         ListView.builder(
      itemCount: 2,
      itemBuilder: (context, index) {
        return Card(
          margin: EdgeInsets.all(5),
          child:
         Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(16.0, 16.0, 8.0, 16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          new Container(
                            height: 40.0,
                            width: 40.0,
                            decoration: new BoxDecoration(
                              shape: BoxShape.circle,
                              image: new DecorationImage(
                                  fit: BoxFit.fill,
                                  image: new NetworkImage(
                                      "https://pbs.twimg.com/profile_images/916384996092448768/PF1TSFOE_400x400.jpg")),
                            ),
                          ),
                          new SizedBox(
                            width: 10.0,
                          ),
                        Column(
  mainAxisAlignment: MainAxisAlignment.start,
  crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            new Text(
                            "مصطفي احمد ",
                            style: TextStyle(fontWeight: FontWeight.normal,fontSize: TitleFontSize ),
                          ),
                          Text('20-2-2021 8:46 AM',style: TextStyle(color: ErrorColor),)])
                        ],
                      ),
                      
                      new IconButton(
                        icon: Icon(Icons.more_vert),
                        onPressed: null,
                      )
                    ],
                  ),
                ),
                 new Text(
                            "الماده  :   رياضيات صف ثان ابتائي ",
                            style: TextStyle(fontWeight: FontWeight.normal),
                          ),
                          new Text(
                            "الدرس   :  الاحاد والعاشرات    ",
                            style: TextStyle(fontWeight: FontWeight.normal),
                          ),
                          SizedBox(height:10),
                Flexible(
                  fit: FlexFit.loose,
                  child: new Image.network(
                    "https://assets.entrepreneur.com/content/3x2/2000/20191219170611-GettyImages-1152794789.jpeg",
                    fit: BoxFit.cover,
                  ),
                ),
                 Padding(
                  padding: const EdgeInsets.all(1.0),
                  child:Container(child:Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("٦ الاعجابات"),
                      Text("٠ التقييم ")

                    ]))),
                Padding(
                  padding: const EdgeInsets.all(0.0),
                  child:Container(child:Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new IconButton(
                            icon: new Icon(isPressed
                                ? FontAwesomeIcons.handHoldingHeart
                                : FontAwesomeIcons.handPointLeft),
                            color: isPressed ? Colors.red : Colors.black,
                            onPressed: () {
                              setState(() {
                                isPressed = !isPressed;
                              });
                            },
                          ),

                          Text("الاعجاب ")
                        ],
                      ),
                      Container(color: Colors.grey[200],width: 2,height: 34,),
                     Row(children: [
                        new Icon(FontAwesomeIcons.star),
                        SizedBox(width:5),
                          Text("التقييم ")

                    
                    ])],
                    
                    ),
                      decoration: BoxDecoration(
                         border: Border.all(color: ErrorColor),
                        borderRadius: BorderRadius.circular(1),
                      ),
                  ),
                ),
                ],
        ));
        }
    ))]));
  
  }
}
