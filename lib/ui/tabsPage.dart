import 'package:flutter/material.dart';
import 'package:schooler/ui/home.dart';
import 'package:schooler/uitils/const_color.dart';

class TabsMainPage extends StatefulWidget {
  TabsMainPage({Key key}) : super(key: key);

  @override
  _TabsMainPageState createState() => _TabsMainPageState();
}

class _TabsMainPageState extends State<TabsMainPage> {
  int _selectedIndex = 2;
  // static const TextStyle optionStyle =
  //     TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
    List _widgetOptions = [
   HomePage(),
     HomePage(),
     HomePage(),
      HomePage(),
       HomePage(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
     
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: PrimaryColor,
        unselectedItemColor: Colors.black,
        
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            
            icon: Icon(Icons.email),
            title: Text('البريد' ),
          ),
            BottomNavigationBarItem(
            icon: Icon(Icons.notifications),
            title: Text('الاشعارات' ),
          ),
            BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('الرئيسية' ),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.book_sharp),
            title: Text('المواد'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.library_books),
                       title: Text('المكتبة الالكترونية'),

          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: PrimaryColor,
        onTap: _onItemTapped,
      ),
    );
  }
}