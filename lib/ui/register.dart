import 'package:flutter/material.dart';
import 'package:schooler/ui/widget/widgetRaisedButton.dart';
import 'package:schooler/ui/widget/widgetTextField.dart';
import 'package:schooler/uitils/change_route.dart';
import 'package:schooler/uitils/const_color.dart';

import 'home.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  TextEditingController fullName = new TextEditingController();
  TextEditingController phone = new TextEditingController();
  TextEditingController email = new TextEditingController();
  TextEditingController password = new TextEditingController();

  double radiusValue = 30;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: PrimaryLightColor,
      body: Container(
          padding: EdgeInsets.all(20),
          child: ListView(
              // mainAxisAlignment: MainAxisAlignment.center,
              // crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                    color: PrimaryLightColor,
                    child: Image.asset(
                      'assets/logo.png',
                      height: 100,
                      width: 260,
                    )
                    // ),
                    ),
                Container(
                  height: 100,
                  width: 50,
                  margin: EdgeInsets.only(left: 130,right:130),
                   decoration: BoxDecoration(
            color: AccentColor,
            border: Border.all(color: ErrorColor),
            borderRadius: BorderRadius.circular(200),
            image: DecorationImage(
            image: NetworkImage("https://static.thenounproject.com/png/990460-200.png"),
            fit: BoxFit.cover,
          ),
          ),
                ),
                 sizeBox(),
                Container(
                    color: PrimaryLightColor,
                    child: widgetTextField(
                        hintText: 'اسم الطالب رباعي',
                        labelText: 'اسم الطالب رباعي',
                        controller: fullName,
                        textInput: TextInputType.name,
                        radius: radiusValue)),
                sizeBox(),
                Container(
                    color: PrimaryLightColor,
                    child: widgetTextField(
                        hintText: 'رقم الجوال ',
                        labelText: 'رقم الجوال',
                        controller: phone,
                        textInput: TextInputType.phone,
                        radius: radiusValue)),
                sizeBox(),
                Container(
                    color: PrimaryLightColor,
                    child: widgetTextField(
                        hintText: 'البريد الإلكتروني ',
                        labelText: 'البريد الإلكتروني',
                        controller: email,
                        textInput: TextInputType.emailAddress,
                        radius: radiusValue)),
                sizeBox(),
                Container(
                    color: PrimaryLightColor,
                    child: widgetTextField(
                        hintText: 'كلمة المرور ',
                        labelText: 'كلمة المرور',
                        controller: password,
                        textInput: TextInputType.name,
                        radius: radiusValue)),
                sizeBox(),
                dropList('ارفاق شهادة ميلاد الطالب'),
                sizeBox(),
                dropList('ارفاق الهوية الشخصية لولي الامر'),
                sizeBox(),
                dropList('ارفاق جواز السفر لولي الامر'),
                sizeBox(),
                Text('بالنقر علي هذا الزر أوكد أن البيانات المرسلة صحيحة',style: TextStyle(fontSize: 14),textAlign: TextAlign.center,),
                SizedBox(height: 25),

                Container(
                    width: 170,
                    height: 50,
                    child: raisedButton(
                        buttonColor: PrimaryDarkColor,
                        ontap: () {
                          changeScreenRemoveUntil(context, HomePage());
                        },
                        labelText: 'انشاء الطلب ')),
              ])),
    );
  }

  Widget sizeBox() {
    return SizedBox(height: 10);
  }

  Widget dropList(String hint) {
    return Container(
      color: PrimaryLightColor,
      // height: 50,
      child: Container(
          // margin: EdgeInsets.only(left: 15, right: 15),
          padding: EdgeInsets.only(left: 5, right: 5, top: 3, bottom: 3),
          decoration: BoxDecoration(
            color: AccentColor,
            borderRadius: BorderRadius.circular(25),
          ),
          child: new DropdownButton<String>(
            isExpanded: true,
            underline: Container(
              height: 0,
            ),
            hint: Center(
                child: Text(
              '---' + hint + '---',
              style: TextStyle(fontSize: 15),
            )),
            items: <String>[
              'المدرسة الاهلية الخاصة',
              'المدرسة الاهلية',
              'المدرسة الاهلية الخاصة',
              'المدرسة الاهلية'
            ].map((String value) {
              return new DropdownMenuItem<String>(
                value: value,
                child: new Text(value),
              );
            }).toList(),
            onChanged: (_) {},
          )),
    );
  }
}
