import 'package:flutter/material.dart';
import 'package:schooler/ui/widget/layoutWidget.dart';
import 'package:schooler/ui/widget/listAppwidget.dart';
import 'package:table_calendar/table_calendar.dart';

class WeeklyPlanPage extends StatefulWidget {
  @override
  _WeeklyPlanPageState createState() => _WeeklyPlanPageState();
}

class _WeeklyPlanPageState extends State<WeeklyPlanPage> {
CalendarController _calendarController;
@override
void initState() {
  super.initState();
  _calendarController = CalendarController();
}

@override
void dispose() {
  _calendarController.dispose();
  super.dispose();
}

@override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWidget(title: " الخطة الاسبوعية    ", context: context),
      body: Column(children: [
       Container(
         height:180
         ,child:   TableCalendar(
        locale: 'ar',
    calendarController: _calendarController,
  )),
   Expanded(
          child: ListView.builder(
        itemCount: 6,
        itemBuilder: (context, index) {
          return weeklyPlanWidget(
              title: "الاعادادات الصحية والقيمة المطلقة  ",
              subtitle: " الرجاء المتابعة والشرح لدرس ف الصحة الافتراضية ومحاضرات الفديو والمكتبية الرقمية والمحلية الرجاء المتابعة والشرح لدرس ف الصحة الافتراضية ومحاضرات الفديو والمكتبية الرقمية والمحلية ",
              );
        },
      )),
  ],));

  }
}
