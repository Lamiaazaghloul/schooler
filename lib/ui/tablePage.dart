import 'package:flutter/material.dart';
import 'package:schooler/ui/widget/layoutWidget.dart';
import 'package:schooler/ui/widget/listAppwidget.dart';
import 'package:schooler/uitils/const_color.dart';

class TablePage extends StatefulWidget {
  @override
  _TablePageState createState() => _TablePageState();
}
 
class _TablePageState extends State<TablePage> {
    List tabs=[' الاحد ','الاثنين','الثلاثاء ','الاربعاء','الخميس','الجمعة','السبت'];
     int _selectedIndex = 0;
    _onSelected(int index) {
        setState(() => _selectedIndex = index);
        print(index);
    }
  @override
  Widget build(BuildContext context) {
     return Scaffold(
      appBar: appBarWidget(title: " الجدول ", context: context),
      body:Column(children: [
        
        Container(
          margin: EdgeInsets.symmetric(vertical: 20.0),
          height: 40.0,
          child: ListView.builder(
             scrollDirection: Axis.horizontal,
             shrinkWrap: false,
      itemCount: tabs.length,
      itemBuilder: (context, index) {
 return InkWell(
     onTap: () => _onSelected(index),
     child:Container(
     decoration: BoxDecoration(
     color: _selectedIndex != null && _selectedIndex == index
                ? PrimaryColor
                : ErrorColor,
          borderRadius: BorderRadius.circular(50),
     ),
     padding: EdgeInsets.only(left: 15,right: 15,top: 4),
       margin: EdgeInsets.only(left: 5,right: 5),
     child:Text(tabs[index],style: TextStyle(color: AccentColor),textAlign: TextAlign.center,),
  ));
            }
)),
        Expanded(child:
        
   
        //    ListView.builder(
        // itemCount: 7,
        // itemBuilder: (context, index) {
          // return 
          ListView(children: [
          tableWidget(
              title: "الحصة الاولي",
              subtitle: 'التوحيد'
             
          ),
           tableWidget(
              title: "الحصة التانية",
              subtitle: '    الدين'
             
          ),
           tableWidget(
              title: "الحصة التالته",
              subtitle: 'رياضيات'
             
          ),
           tableWidget(
              title: "الحصة الرابعة",
              subtitle: '   اللعاب'
             
          ),
           tableWidget(
              title: "الحصة الخامسة",
              subtitle: 'عربي'
             
          ),
           tableWidget(
              title: "الحصة السادسة",
              subtitle: 'دراسات'
             
          ),
           tableWidget(
              title: "الحصة السابعة",
              subtitle: 'التوحيد'
             
          )],)
        
      
      )]),
    );
  }
}
