import 'package:flutter/material.dart';
import 'package:schooler/ui/widget/layoutWidget.dart';
import 'package:schooler/ui/widget/listAppwidget.dart';


class SubjectPage extends StatefulWidget {
  @override
  _SubjectPageState createState() => _SubjectPageState();
}

class _SubjectPageState extends State<SubjectPage> {
  @override
  Widget build(BuildContext context) {
     return Scaffold(
      appBar: appBarWidget(title: "المواد ", context: context),
      body: Container(
          child: ListView.builder(
        itemCount: 2,
        itemBuilder: (context, index) {
          return subjectWidget(
              title: "اجتماعيات",
              subtitle: "رياض حسام ",
              );
        },
      )),
    );
  }
}
