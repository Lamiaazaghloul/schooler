
import 'package:flutter/material.dart';
import 'package:schooler/ui/Absences.dart';
import 'package:schooler/ui/ArticleAttachments.dart';
import 'package:schooler/ui/Exams.dart';
import 'package:schooler/ui/login.dart';
import 'package:schooler/ui/tablePage.dart';
import 'package:schooler/ui/tabsPage.dart';
import 'package:schooler/ui/weeklyPlan.dart';
import 'package:schooler/uitils/change_route.dart';
import 'package:schooler/uitils/const_color.dart';

import 'AssignmentsElectronic.dart';
import 'Behaviors.dart';
import 'DiscussionRoom.dart';
import 'Subjects.dart';
import 'VideoLectures.dart';
import 'VirtualClassroom.dart';
import 'emails.dart';

class DrawerPage extends StatefulWidget {
  @override
  _DrawerPageState createState() => _DrawerPageState();
}

class _DrawerPageState extends State<DrawerPage> {
  @override
  Widget build(BuildContext context) {
   return new Drawer(
      child: ListView(
        children: <Widget>[
            Container(
          height: 120,
          child:DrawerHeader(
          child:Row(children: [
                Image.network('http://getdrawings.com/free-icon/icon-student-59.png',width: 70,height:70,)
              ,Column(
             mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
            children: [ 
              
            Text('محمد عبد الرحمن',style: TextStyle(fontWeight: FontWeight.normal,color: PrimaryLightColor,fontSize: 14),),
            Text('متوسطة الاخلاص الاهلية',style: TextStyle(fontWeight: FontWeight.normal,color: PrimaryLightColor,fontSize: 14),)
           , Text('* 19428',style: TextStyle(fontWeight: FontWeight.normal,color: PrimaryLightColor,fontSize: 14),)
            
            ],)],),),
        decoration: BoxDecoration(
          color: PrimaryColor,
        ),
      ),
          widgetListItem(ontap: (){
             changeScreenPush(context,TabsMainPage());
          },
          image: 'assets/home.png',
          lable: 'الرئيسية'),
          Divider(),

           widgetListItem(ontap: (){  
             
              changeScreenPush(context,AssignmentsElectronicPage());
           },
          image: 'assets/homework.png',
          lable: 'الواجبات الالكترونية'),
          Divider(),

           widgetListItem(ontap: (){
              changeScreenPush(context,ArticleAttachmentsPage());

             
           },
          image: 'assets/hat.png',
          lable: 'مرفقات المادة'),
          Divider(),

           widgetListItem(ontap: (){
             
             changeScreenPush(context,ExamsPage());
           },
          image: 'assets/exam.png',
          lable: 'الامتحانات '),
          Divider(),
            widgetListItem(ontap: (){
             changeScreenPush(context,VideoLecturesPage());

            },
          image: 'assets/lect_online.png',
          lable: 'محاضرات الفديو'),
          Divider(),


           widgetListItem(ontap: (){
             changeScreenPush(context,EmailsPage());

             
           },
          image: 'assets/mail.png',
          lable: 'البريد '),
          Divider(),

           widgetListItem(ontap: (){
             
             changeScreenPush(context,AssignmentsElectronicPage());

           },
          image: 'assets/rate.png',
          lable: 'التقيمات '),
          Divider(),

           widgetListItem(ontap: (){
             changeScreenPush(context,DiscussionRoomPage());
           },
          image: 'assets/descation.png',
          lable: 'غرف النقاش'),
          Divider(),
           widgetListItem(ontap: (){
             changeScreenPush(context,WeeklyPlanPage());
           },
          image: 'assets/calender.png',
          lable: 'الخطة الاسبوعية '),
          Divider(),

           widgetListItem(ontap: (){
              changeScreenPush(context,Behaviors());
           },
          image: 'assets/group.png',
          lable: 'السلوكيات '),
          Divider(),

           widgetListItem(ontap: (){
              changeScreenPush(context,TabsMainPage());
           },
          image: 'assets/calender.png',
          lable: 'التقويم الاكاديمي'),
          Divider(),

           widgetListItem(ontap: (){
              changeScreenPush(context,TablePage());
           },
          image: 'assets/table.png',
          lable: 'الجدول'),
          Divider(),
            widgetListItem(ontap: (){
               changeScreenPush(context,AbsencesPage());
            },
          image: 'assets/calender.png',
          lable: 'الغياب'),
          Divider(),
            widgetListItem(ontap: (){
               changeScreenPush(context,VirtualClassroomPage());
            },
          image: 'assets/descation.png',
          lable: 'الغرفة الصفية الافتراضية'),
          Divider(),

            widgetListItem(ontap: (){
               changeScreenPush(context,SubjectPage());
            },
          image: 'assets/hat.png',
          lable: 'المواد'),
          Divider(),
            widgetListItem(ontap: (){
               changeScreenPush(context,TabsMainPage());
            },
          image: 'assets/report.png',
          lable: 'التقارير'),
          Divider(),
            widgetListItem(ontap: (){
               changeScreenPush(context,TabsMainPage());
            },
          image: 'assets/card.png',
          lable: 'بطاقتي'),
          Divider(),

  widgetListItem(ontap: (){
     changeScreenPush(context,TabsMainPage());
  },
          image: 'assets/ads.png',
          lable: 'الاعلانات'),
          Divider(),

            widgetListItem(ontap: (){
               changeScreenPush(context,TabsMainPage());
            },
          image: 'assets/file.png',
          lable: 'الملف الشخصي الالكتروني'),
          Divider(),

            widgetListItem(ontap: (){
             
            },
          image: 'assets/group.png',
          lable: 'شركاء النجاح'),
          Divider(),

            widgetListItem(ontap: (){
             
            },
          image: 'assets/setting.png',
          lable: 'الاعدادات'),
          Divider(),
           widgetListItem(ontap: (){
              changeScreenPush(context,Login());
           },
          image: 'assets/logout.png',
          lable: 'الخروج'),
          Divider(),

          ]
      )
    );
  }




  Widget widgetListItem({Function ontap,String lable,String image}){
return new Container(child: ListTile(
            onTap: () {
               Navigator.pop(context);
             ontap();
            },
            title: Text(lable,style: TextStyle(color: LableColor,fontSize: 15,fontWeight: FontWeight.normal),),
            leading: new Image.asset(image,width: 25,),
            trailing: Icon(Icons.arrow_forward_ios,size: 20,),
 ),
 height: 45, );
  }
}