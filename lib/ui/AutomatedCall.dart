import 'package:flutter/material.dart';
import 'package:schooler/ui/widget/listAppwidget.dart';
import 'package:schooler/uitils/const_color.dart';

import 'widget/layoutWidget.dart';

class AutomatedCallPage extends StatefulWidget {
  @override
  _AutomatedCallPageState createState() => _AutomatedCallPageState();
}

class _AutomatedCallPageState extends State<AutomatedCallPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWidget(title: "النداء الألي", context: context),
      body: Container(
          child: ListView.builder(
        itemCount: 5,
        itemBuilder: (context, index) {
          return automatedListWidget(
              title: "Lamiaa zaghloul",
              subtitle: "طالب ١ ",
              imageWisget: CircleAvatar(
                // backgroundImage: NetworkImage('https://images.assetsdelivery.com/compings_v2/koblizeek/koblizeek1901/koblizeek190100017.jpg'),
                radius: 35,
                backgroundColor: PrimaryColor,
              ),
              time: "10:10 PM");
        },
      )),
    );
  }
}
