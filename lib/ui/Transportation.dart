import 'package:flutter/material.dart';
import 'package:schooler/ui/widget/layoutWidget.dart';
import 'package:schooler/ui/widget/listAppwidget.dart';
import 'package:schooler/uitils/const_color.dart';

class TransportationPage extends StatefulWidget {
  @override
  _TransportationPageState createState() => _TransportationPageState();
}

class _TransportationPageState extends State<TransportationPage> {
  @override
  Widget build(BuildContext context) {
     return Scaffold(
      appBar: appBarWidget(title: "المواصلات ", context: context),
      body: Container(
          child: ListView.builder(
        itemCount: 2,
        itemBuilder: (context, index) {
          return transportationWidget(
              title: "الرسالة لتوصيل",
              subtitle: "عدد الركاب ١ ",
              imageWisget: CircleAvatar(
                // backgroundImage: NetworkImage('https://images.assetsdelivery.com/compings_v2/koblizeek/koblizeek1901/koblizeek190100017.jpg'),
                radius: 35, 
                backgroundColor: PrimaryDarkColor,
              ),
              mony: "500 ريال",
              lableValue: "قيمة الاشتراك ");
        },
      )),
    );
  }
}
