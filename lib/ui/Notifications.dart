import 'package:flutter/material.dart';
import 'package:schooler/ui/widget/layoutWidget.dart';
import 'package:schooler/ui/widget/listAppwidget.dart';


class NotificationsPage extends StatefulWidget {
  @override
  _NotificationsPageState createState() => _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage> {
  @override
  Widget build(BuildContext context) {
     return Scaffold(
      appBar: appBarWidget(title: "الاشعارات ", context: context),
      body: Container(
          child: ListView.builder(
        itemCount: 6,
        itemBuilder: (context, index) {
          return notifyWidget(
              title: "واجب جديد تابع الحضارات القديمه ",
              subtitle: "٢٠-٢-٢٠٢١ ٨:٠٠  ",
              );
        },
      )),
    );
  }
}
