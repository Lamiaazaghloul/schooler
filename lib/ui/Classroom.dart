import 'package:flutter/material.dart';
import 'package:schooler/ui/widget/layoutWidget.dart';
import 'package:schooler/ui/widget/listAppwidget.dart';


class ClassRoomPage extends StatefulWidget {
  @override
  _ClassRoomPageState createState() => _ClassRoomPageState();
}

class _ClassRoomPageState extends State<ClassRoomPage> {
  @override
  Widget build(BuildContext context) {
     return Scaffold(
      appBar: appBarWidget(title: "الفصول الدراسية ", context: context),
      body: Container(
          child: ListView.builder(
        itemCount: 2,
        itemBuilder: (context, index) {
          return examWidget(
              title: "مدرس",
              subtitle: "رياض حسام ",
              lableValue: "الاول ( ١ ) ");
        },
      )),
    );
  }
}
