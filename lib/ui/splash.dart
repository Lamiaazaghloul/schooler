import 'dart:async';

import 'package:flutter/material.dart';
import 'package:schooler/uitils/change_route.dart';

import 'Introduction.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<SplashPage> {

   @override
  void initState() {
    super.initState();
    loadStartPage();
  }

  Future<Timer> loadStartPage() async {
    return new Timer(Duration(seconds: 5), onDoneLoading);
  }

  onDoneLoading() async {
    changeScreenReplacementPage(context,Interodaction());
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      width: double.infinity,
      height: double.infinity,
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Image.asset(
            'assets/splash.png',
            fit: BoxFit.cover,
          ),

        ],
      ),
    ));
 
  }
  
}