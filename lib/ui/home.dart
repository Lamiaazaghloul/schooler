import 'package:flutter/material.dart';
import 'package:flutter_circular_chart/flutter_circular_chart.dart';
import 'package:schooler/ui/Notifications.dart';
import 'package:schooler/ui/sidemenu.dart';
import 'package:schooler/uitils/change_route.dart';
import 'package:schooler/uitils/const_color.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final GlobalKey<AnimatedCircularChartState> _chartKey = new GlobalKey<AnimatedCircularChartState>();
  final GlobalKey<AnimatedCircularChartState> _chartKey2 = new GlobalKey<AnimatedCircularChartState>();
  final GlobalKey<AnimatedCircularChartState> _chartKey3 = new GlobalKey<AnimatedCircularChartState>();
  final GlobalKey<AnimatedCircularChartState> _chartKey4 = new GlobalKey<AnimatedCircularChartState>();
  final GlobalKey<AnimatedCircularChartState> _chartKey5 = new GlobalKey<AnimatedCircularChartState>();
  final GlobalKey<AnimatedCircularChartState> _chartKey6 = new GlobalKey<AnimatedCircularChartState>();
  final GlobalKey<AnimatedCircularChartState> _chartKey7 = new GlobalKey<AnimatedCircularChartState>();
  final GlobalKey<AnimatedCircularChartState> _chartKey8 = new GlobalKey<AnimatedCircularChartState>();
  
  @override
  Widget build(BuildContext context) {
   return Scaffold(
     backgroundColor: BackGroundColor,
     drawer: DrawerPage(),
     
     appBar: AppBar(
       title:Center(child:Text("الرئيسية",style: TextStyle(color: Colors.white,fontSize: 16,))),
     iconTheme: new IconThemeData(color: Colors.white),
     backgroundColor: PrimaryColor,
     actions:[ InkWell(
       onTap: (){
             changeScreenPush(context,NotificationsPage());

       },
       child:Container(
       padding: EdgeInsets.only(left: 20),
       child:Icon(Icons.notifications,color: Colors.white,)),)],
     ),
     body:Container(child: 
  
     GridView.count(
  primary: false,
  padding: const EdgeInsets.all(25),
  crossAxisSpacing: 15,
  mainAxisSpacing: 15,
  crossAxisCount: 2,
  children: <Widget>[
itemList(_chartKey,'Key1','Key2','progress','الاختبارات ',Colors.purple),
itemList(_chartKey2,'Key12','Key22','progress2','الواجبات',Colors.greenAccent),
itemList(_chartKey3,'Key13','Key23','progres3','الغرف الصفية الافتراضية',Colors.pinkAccent),
itemList(_chartKey4,'Key14','Key24','progress4','محاضرات الفديو',Colors.indigoAccent),
itemList(_chartKey5,'Key15','Key25','progress5','التقارير',Colors.grey),
itemList(_chartKey6,'Key16','Key26','progress6','غرف النقاش',Colors.orange),
itemList(_chartKey7,'Key17','Key27','progress7','التقيمات',Colors.blue),
itemList(_chartKey8,'Key18','Key28','progress8','التقويم الاكاديمي',Colors.green)
  ])
   ,));
  }


  Widget itemList(chartKey,rankKey1,rankKey,progress,name,Color color){
    return new Container(
       width: MediaQuery.of(context).size.width,
      child:Stack(
        // alignment: Alignment.center,
    children: <Widget>[
       Container(
      // padding: EdgeInsets.all(10),
     width: MediaQuery.of(context).size.width,
     height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
                        color: PrimaryLightColor,
                        borderRadius: BorderRadius.circular(20),
                      ),
      child:Column(
        mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
      children: [
       new AnimatedCircularChart(
  key: chartKey,
  size: Size(140,140),
  initialChartData: <CircularStackEntry>[
    new CircularStackEntry(
      <CircularSegmentEntry>[
        new CircularSegmentEntry(
          33.33,
          color,
          rankKey: 'completed',
        ),
        new CircularSegmentEntry(
          66.67,
          Colors.grey[200],
          rankKey: 'remaining',
        ),
      ],
      rankKey: 'progress',
    ),
  ],
  chartType: CircularChartType.Radial,
  percentageValues: true,
  holeLabel: '1/3',
  
  labelStyle: new TextStyle(
    color: Colors.blueGrey[600],
    fontWeight: FontWeight.bold,
    fontSize: 24.0,
  ),
),
Text(name)
])
  ),
  
   new Positioned(
        left: 0,
        child: new Container(
          padding: EdgeInsets.only(right:10,left: 10,bottom: 5,top: 5),
          decoration: new BoxDecoration(
            color: Colors.red,
             borderRadius: BorderRadius.only(bottomRight:  Radius.circular(15)),
        border: Border.all(width: 0,color: Colors.green,style: BorderStyle.solid)
          ),
          constraints: BoxConstraints(
            minWidth: 12,
            minHeight: 12,
          ),
          child: new Text(
            '1',
            style: new TextStyle(
              color: Colors.white,
              fontSize: 10,
            ),
            textAlign: TextAlign.center,
          ),
        ),
      )
      ]));
  }
}