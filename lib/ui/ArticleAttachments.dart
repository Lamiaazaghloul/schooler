import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:schooler/ui/widget/layoutWidget.dart';
import 'package:schooler/uitils/const_color.dart';


class ArticleAttachmentsPage extends StatefulWidget {
  @override
  _ArticleAttachmentsPageState createState() => _ArticleAttachmentsPageState();
}

class _ArticleAttachmentsPageState extends State<ArticleAttachmentsPage> {


bool isPressed = false;

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: appBarWidget(title: "مرفقات المادة ", context: context),
      body: Column(children: [
        
       Expanded(child:
         ListView.builder(
      itemCount: 2,
      itemBuilder: (context, index) {
        return Card(
          margin: EdgeInsets.all(5),
          child:
         Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(16.0, 16.0, 8.0, 16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          new Container(
                            height: 40.0,
                            width: 40.0,
                            decoration: new BoxDecoration(
                              shape: BoxShape.circle,
                              image: new DecorationImage(
                                  fit: BoxFit.fill,
                                  image: new NetworkImage(
                                      "https://pbs.twimg.com/profile_images/916384996092448768/PF1TSFOE_400x400.jpg")),
                            ),
                          ),
                          new SizedBox(
                            width: 10.0,
                          ),
                        Column(
  mainAxisAlignment: MainAxisAlignment.start,
  crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            new Text(
                            "مصطفي احمد ",
                            style: TextStyle(fontWeight: FontWeight.normal,fontSize: TitleFontSize ),
                          ),
                          Text('20-2-2021 8:46 AM',style: TextStyle(color: ErrorColor),)])
                        ],
                      ),
                      
                     
                    ],
                  ),
                ),
                 new Text(
                            "الماده  :   رياضيات صف ثان ابتائي ",
                            style: TextStyle(fontWeight: FontWeight.normal),
                          ),
                          new Text(
                            "الدرس   :  الاحاد والعاشرات    ",
                            style: TextStyle(fontWeight: FontWeight.normal),
                          ),
                          SizedBox(height:10),
                Flexible(
                  fit: FlexFit.loose,
                  child: new Image.network(
                    "https://assets.entrepreneur.com/content/3x2/2000/20191219170611-GettyImages-1152794789.jpeg",
                    fit: BoxFit.cover,
                  ),
                ),
                 Padding(
                  padding: const EdgeInsets.all(1.0),
                  child:Container(child:Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("٦ الاعجابات"),
                      Text("٠ التقييم ")

                    ]))),
                Padding(
                  padding: const EdgeInsets.all(0.0),
                  child:Container(
                    child:Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new IconButton(
                            icon: new Icon(isPressed
                                ? FontAwesomeIcons.handHoldingHeart
                                : FontAwesomeIcons.handPointLeft),
                            color: isPressed ? Colors.red : Colors.black,
                            onPressed: () {
                              setState(() {
                                isPressed = !isPressed;
                              });
                            },
                          ),

                          Text("الاعجاب ")
                        ],
                      ),
                      Container(color: ErrorColor,width: 2,height: 34,),
                     Row(children: [
                        new Icon(FontAwesomeIcons.star),
                        SizedBox(width:5),
                          Text("التقييم ")

                    
                    ])],
                    
                    ),
                      decoration: BoxDecoration(
                         border: Border.all(color: Colors.grey[200]),
                        borderRadius: BorderRadius.circular(1),
                      ),
                  ),
                ),
                ],
        ));
        }
    ))]));
  
  }
}
