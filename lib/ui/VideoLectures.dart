import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:schooler/ui/widget/layoutWidget.dart';
import 'package:schooler/uitils/const_color.dart';
import 'package:video_player/video_player.dart';


class VideoLecturesPage extends StatefulWidget {
  @override
  _VideoLecturesPageState createState() => _VideoLecturesPageState();
}

class _VideoLecturesPageState extends State<VideoLecturesPage> {


bool isPressed = false;
VideoPlayerController _controller;

  @override
  void initState() {
    super.initState();
  _controller = VideoPlayerController.network(
        'http://www.sample-videos.com/video123/mp4/720/big_buck_bunny_720p_20mb.mp4')
      ..initialize().then((_) {
        // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
        setState(() {});
      });
  }
  
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: appBarWidget(title: " محاضرات الفديو ", context: context),
      body: Column(children: [
        
       
        Expanded(child:
         ListView.builder(
      itemCount: 2,
      itemBuilder: (context, index) {
        return Card(
          margin: EdgeInsets.all(5),
          child:
         Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(16.0, 16.0, 8.0, 16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          new Container(
                            height: 40.0,
                            width: 40.0,
                            decoration: new BoxDecoration(
                              shape: BoxShape.circle,
                              image: new DecorationImage(
                                  fit: BoxFit.fill,
                                  image: new NetworkImage(
                                      "https://pbs.twimg.com/profile_images/916384996092448768/PF1TSFOE_400x400.jpg")),
                            ),
                            
                          ),
                          new SizedBox(
                            width: 10.0,
                          ),
                        Column(
  mainAxisAlignment: MainAxisAlignment.start,
  crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            new Text(
                            "مصطفي احمد ",
                            style: TextStyle(fontWeight: FontWeight.normal,fontSize: TitleFontSize ),
                          ),
                          Text('20-2-2021 8:46 AM',style: TextStyle(color: ErrorColor),)])
                        ],
                      ),
                      
                      new IconButton(
                        icon: Icon(Icons.more_vert),
                        onPressed: null,
                      )
                    ],
                  ),
                ),
                 new Text(
                            "الماده  :   رياضيات صف ثان ابتائي ",
                            style: TextStyle(fontWeight: FontWeight.normal),
                          ),
                          new Text(
                            "الدرس   :  الاحاد والعاشرات    ",
                            style: TextStyle(fontWeight: FontWeight.normal),
                          ),
                          SizedBox(height:10),
                Flexible(
                  fit: FlexFit.loose,
                  child:Container(
                    height: 200,
          child: _controller.value.initialized
              ? AspectRatio(
                  aspectRatio: _controller.value.aspectRatio,
                  child: VideoPlayer(_controller),
                )
              : Container(child: Image.network('https://thesweetbits.com/wp-content/uploads/2019/07/Video-Player.jpg'),),
        ),
                ),
                 Padding(
                  padding: const EdgeInsets.all(1.0),
                  child:Container(child:Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("٦ الاعجابات"),
                      Text("٠ التقييم ")

                    ]))),
                Padding(
                  padding: const EdgeInsets.all(0.0),
                  child:Container(child:Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new IconButton(
                            icon: new Icon(isPressed
                                ? FontAwesomeIcons.handHoldingHeart
                                : FontAwesomeIcons.handPointLeft),
                            color: isPressed ? Colors.red : Colors.black,
                            onPressed: () {
                              setState(() {
                                isPressed = !isPressed;
                              });
                            },
                          ),

                          Text("الاعجاب ",textAlign: TextAlign.center)
                        ],
                      ),
                      Container(color: Colors.grey[200],width: 2,height: 34,),
                    Container(
                      margin: EdgeInsets.only(left: 60),
                      child: Row(
                       
                       children: [
                        new Icon(FontAwesomeIcons.star),
                        SizedBox(width:5),
                          Text("التقييم ",textAlign: TextAlign.center,)

                    
                    ]))],
                    
                    ),
                      decoration: BoxDecoration(
                         border: Border(
      top: BorderSide( //                   <--- left side
        color: Colors.grey[200],
        width: 1.0,
      )),
                       
                        // borderRadius: BorderRadius.circular(1),
                      ),
                  ),
                ),
                ],
        ));
        }
    ))]));
  
  }
}
