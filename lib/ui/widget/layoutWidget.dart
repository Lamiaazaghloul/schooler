
import 'package:flutter/material.dart';
import 'package:schooler/uitils/const_color.dart';

Widget appBarWidget({title,Widget actionWidget,context}){
  return AppBar(title:Center(child:Text(title,style: TextStyle(fontSize: 16,color: Colors.white),)),
  backgroundColor: PrimaryColor,
  leading: InkWell(onTap: () {
    Navigator.pop(context);
  },
  child:Icon(Icons.arrow_back_ios,color:Colors.white)),
  actions: [actionWidget==null?Container():actionWidget],

  );
}