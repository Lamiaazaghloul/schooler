import 'package:flutter/material.dart';
import 'package:getwidget/colors/gf_color.dart';
import 'package:getwidget/components/progress_bar/gf_progress_bar.dart';
import 'package:schooler/uitils/const_color.dart';

Widget automatedListWidget({title,subtitle,imageWisget,time}){
  return Card(
   margin: EdgeInsets.only(left:15,right:15,bottom: 5,top:5),
    child:ListTile(title:Text(title),
    leading: imageWisget,
    subtitle: Text(subtitle),
    trailing: Text(time),)
  );
}


Widget transportationWidget({title,subtitle,imageWisget,lableValue,mony,}){
  return Card(
   margin: EdgeInsets.only(left:15,right:15,bottom: 5,top:5),
    child:Column(children: [

    ListTile(title:Text(title,style: TextStyle(fontSize: TitleFontSize),),
    leading: imageWisget,
    subtitle: Text(subtitle,style: TextStyle(fontSize: SubTitleFontSize),),
   ),

    Container(
      margin: EdgeInsets.all(10),
      color: GreyColor,
      child:
     ListTile(title:Text(lableValue,style: TextStyle(fontSize: TitleFontSize)),
    leading: imageWisget,
    subtitle: Text(mony,style: TextStyle(fontSize: SubTitleFontSize)),
    ))
    ],)
  );
}

Widget examWidget({title,subtitle,lableValue,}){
  return Card(
   margin: EdgeInsets.only(left:15,right:15,bottom: 5,top:5),
   child:Stack(children: [
Container(color:GreyColor,
margin: EdgeInsets.only(top: 40,left: 5,right: 5,bottom: 5),
child:
ListTile(title:Text(title,style: TextStyle(fontSize: TitleFontSize),
),
leading:Container(
  padding: EdgeInsets.all(13),
   decoration: BoxDecoration(
                        color: PrimaryLightColor,
                        borderRadius: BorderRadius.circular(50),
                      ),
  child:Icon(Icons.person,size:25),
),
subtitle:Text( subtitle,style: TextStyle(fontSize: SubTitleFontSize)),)),
Positioned(right: 15,
        child: new Container(
          padding: EdgeInsets.only(right:20,left: 20,bottom: 5,top: 5),
          decoration: new BoxDecoration(
            color: Colors.red,
             borderRadius: BorderRadius.only(bottomRight:  Radius.circular(10),bottomLeft:  Radius.circular(10)),
        border: Border.all(width: 0,color: Colors.green,style: BorderStyle.solid)
          ),
          constraints: BoxConstraints(
            minWidth: 12,
            minHeight: 12,
          ),
          child: new Text(
            lableValue,
            style: new TextStyle(
              color: Colors.white,
              fontSize: 10,
            ),
            textAlign: TextAlign.center,
          ),
        ),
      )

   ],)
  );
}



Widget studentAttendanceWidget({title,subtitle,lableValue,}){
  return Card(
   margin: EdgeInsets.only(left:15,right:15,bottom: 5,top:10),
   child:Column(children: [
       Container(
         height: 60,
     child:ListTile(title: Text('محمد مصطفي',style: TextStyle(fontSize: TitleFontSize),),
subtitle:Column(
  mainAxisAlignment: MainAxisAlignment.start,
  crossAxisAlignment: CrossAxisAlignment.start,
  children: [
   Text('الترتيب : ١',style: TextStyle(fontSize: SubTitleFontSize)),
   Text('الفصل الدراسي : الاول (A)',style: TextStyle(fontSize: SubTitleFontSize),
   )],),
   leading:CircleAvatar(
     radius: 25,
     backgroundColor: PrimaryColor,
  backgroundImage: NetworkImage('https://img2.pngio.com/united-states-avatar-organization-information-png-512x512px-user-avatar-png-820_512.jpg'),
) ),),

Container(
  color:GreyColor,
margin: EdgeInsets.only(top: 40,left: 5,right: 5,bottom: 5),
child:
ListTile(title:Text(title,style: TextStyle(fontSize: TitleFontSize),
),
leading:Container(
  padding: EdgeInsets.all(13),
   decoration: BoxDecoration(
                        color: PrimaryColor,
                        borderRadius: BorderRadius.circular(50),
                      ),
  child:Icon(Icons.person,size:25,color: PrimaryLightColor,),
),
subtitle:Text( subtitle,style: TextStyle(fontSize: SubTitleFontSize)),)),
      Stack(children: [

        
Container(color:GreyColor,
margin: EdgeInsets.only(top: 0,left: 5,right: 5,bottom: 5),
child:
ListTile(title:Text('رقم الجوال ',style: TextStyle(fontSize: TitleFontSize),
),
leading:Container(
  padding: EdgeInsets.all(13),
   decoration: BoxDecoration(
                        color: PrimaryColor,
                        borderRadius: BorderRadius.circular(50),
                      ),
  child:Icon(Icons.phone_bluetooth_speaker,size:25,color: PrimaryLightColor,),
),
subtitle:Text( '010101010002',style: TextStyle(fontSize: SubTitleFontSize)),)),
Positioned(bottom: 0,
left: 15,
        child: new Container(
          padding: EdgeInsets.only(right:20,left: 20,bottom: 5,top: 5),
          decoration: new BoxDecoration(
            color: Colors.red,
             borderRadius: BorderRadius.only(topRight:  Radius.circular(10),topLeft:  Radius.circular(10)),
        border: Border.all(width: 0,color: Colors.green,style: BorderStyle.solid)
          ),
          constraints: BoxConstraints(
            minWidth: 12,
            minHeight: 12,
          ),
          child: new Text(
            lableValue,
            style: new TextStyle(
              color: Colors.white,
              fontSize: 10,
            ),
            textAlign: TextAlign.center,
          ),
        ),
      )
],)
   ],)
  );
}



Widget teacherWidget({title,subtitle,lableValue,}){
  return Card(
   margin: EdgeInsets.only(left:15,right:15,bottom: 5,top:10),
   child:Column(children: [
       Container(
         height: 40,
     child:ListTile(title: Text( title,style: TextStyle(fontSize: TitleFontSize),),
subtitle:
   Text(subtitle,style: TextStyle(fontSize: SubTitleFontSize)),
   leading:CircleAvatar(
     radius: 25,
     backgroundColor: PrimaryColor,
  backgroundImage: NetworkImage('https://img2.pngio.com/united-states-avatar-organization-information-png-512x512px-user-avatar-png-820_512.jpg'),
) ),),

Container(
  color:GreyColor,
margin: EdgeInsets.only(top: 40,left: 5,right: 5,bottom: 5),
child:
ListTile(title:Text('البريد الالكتروني',style: TextStyle(fontSize: TitleFontSize),
),
leading:Container(
  padding: EdgeInsets.all(13),
   decoration: BoxDecoration(
                        color: PrimaryColor,
                        borderRadius: BorderRadius.circular(50),
                      ),
  child:Icon(Icons.mail,size:25,color: PrimaryLightColor,),
),
subtitle:Text( 'Lamia@yahoo.com',style: TextStyle(fontSize: SubTitleFontSize)),)),
      Stack(children: [

        
Container(color:GreyColor,
margin: EdgeInsets.only(top: 0,left: 5,right: 5,bottom: 5),
child:
ListTile(title:Text('رقم الجوال ',style: TextStyle(fontSize: TitleFontSize),
),
leading:Container(
  padding: EdgeInsets.all(13),
   decoration: BoxDecoration(
                        color: PrimaryColor,
                        borderRadius: BorderRadius.circular(50),
                      ),
  child:Icon(Icons.phone_bluetooth_speaker,size:25,color: PrimaryLightColor,),
),
subtitle:Text( '010101010002',style: TextStyle(fontSize: SubTitleFontSize)),)),
Positioned(bottom: 0,
left: 15,
        child: new Container(
          padding: EdgeInsets.only(right:20,left: 20,bottom: 5,top: 5),
          decoration: new BoxDecoration(
            color: Colors.red,
             borderRadius: BorderRadius.only(topRight:  Radius.circular(10),topLeft:  Radius.circular(10)),
        border: Border.all(width: 0,color: Colors.green,style: BorderStyle.solid)
          ),
          constraints: BoxConstraints(
            minWidth: 12,
            minHeight: 12,
          ),
          child: new Text(
            lableValue,
            style: new TextStyle(
              color: Colors.white,
              fontSize: 10,
            ),
            textAlign: TextAlign.center,
          ),
        ),
      )
],)
   ],)
  );
}

Widget subjectWidget({title,subtitle,lableValue,}){
  return Card(
   margin: EdgeInsets.only(left:15,right:15,bottom: 5,top:5),
   child:ListTile(title: Text(title,style: TextStyle(fontSize: TitleFontSize),),
   subtitle: Column(
      mainAxisAlignment: MainAxisAlignment.start,
  crossAxisAlignment: CrossAxisAlignment.start,
     children: [
     Text(subtitle,style: TextStyle(fontSize: SubTitleFontSize),),
     SizedBox(height:15),
      Container(child:
   GFProgressBar(
     percentage: 0.8,
     lineHeight: 14,
     alignment: MainAxisAlignment.spaceBetween,
     trailing:Text(' 80 % '),
     backgroundColor: Colors.black26,
     progressBarColor: GFColors.DANGER,
)),
     SizedBox(height:15),
]),
leading:Container(
  padding: EdgeInsets.all(13),
   decoration: BoxDecoration(
                        color: PrimaryColor,
                        borderRadius: BorderRadius.circular(50),
                      ),
  child:Icon(Icons.person,size:25,color: PrimaryLightColor,),
),
)
  );
}

Widget notifyWidget({title,subtitle,}){
  return Card(
   margin: EdgeInsets.only(left:15,right:15,bottom: 5,top:5),
   child:ListTile(title: Text(title,style: TextStyle(fontSize: TitleFontSize),),
   subtitle: Column(
      mainAxisAlignment: MainAxisAlignment.start,
  crossAxisAlignment: CrossAxisAlignment.start,
     children: [
        Text('Homework',style: TextStyle(fontSize: SubTitleFontSize)),

        Text(subtitle,style: TextStyle(fontSize: SubTitleFontSize),
     

   )])));
}
Widget weeklyPlanWidget({title,subtitle,}){
  return Card(
    
   margin: EdgeInsets.only(left:15,right:15,bottom: 5,top:5),
   child:ListTile(title: Text(title,style: TextStyle(fontSize: TitleFontSize),),
   subtitle: Column(
      mainAxisAlignment: MainAxisAlignment.start,
  crossAxisAlignment: CrossAxisAlignment.start,
     children: [

        Text(subtitle,style: TextStyle(fontSize: SubTitleFontSize),
     

   )])));
}


Widget emailsWidget({title,subtitle,date,image}){
  return Card(
    elevation: 0,
   margin: EdgeInsets.only(left:15,right:15,bottom: 5,top:5),
   child:ListTile(title: Text(title,style: TextStyle(fontSize: TitleFontSize),),
   subtitle: Text(subtitle,style: TextStyle(fontSize: SubTitleFontSize)),
    leading: Image.network('https://storage.googleapis.com/stateless-campfire-pictures/2019/05/e4629f8e-defaultuserimage-15579880664l8pc.jpg'),
   trailing: Text('20-10-2020 10:20'),
    ));
}


Widget assignmentsWidget({title,subtitleLable,subtitleValue,dateStart,dateEnd}){
  return Card(
    elevation: 0,
   margin: EdgeInsets.only(left:15,right:15,bottom: 5,top:5),
   child:Column(
       mainAxisAlignment: MainAxisAlignment.start,
  crossAxisAlignment: CrossAxisAlignment.start,
     children: [
   Container(
     padding: EdgeInsets.all(10),
     child:Text(title,style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold)),),
      Container(
              margin: EdgeInsets.all(10),
              child: Table(
                // border: TableBorder.all(width: 0,color: PrimaryColor),
                children: [
                  TableRow( children: [
                      Text('المادة ',textAlign: TextAlign.start,),
                      Text('رياضيات',textAlign: TextAlign.start,style: TextStyle(color: Colors.grey),)
                  ]),

                   TableRow( children: [
                      Text('تاريخ البدء ',textAlign: TextAlign.start,),
                      Text('10-2-2020 10:2',textAlign: TextAlign.start,style: TextStyle(color: Colors.grey))
                  ]),

                   TableRow( children: [
                      Text('تاريخ الايقاف ',textAlign: TextAlign.start,),
                      Text('20-10-2020 10:10',textAlign: TextAlign.start,style: TextStyle(color: Colors.grey))
                  ]),
                 
                ],
              ),
            ),

      Container(
              margin: EdgeInsets.all(10),
              child: Table(
                
                border: TableBorder.all(width: 0,color: PrimaryColor),
                children: [
                  TableRow( children: [
                     
                     InkWell(child: Text('درجات التسليم ',textAlign: TextAlign.center,),
                     onTap: (){},),
                     
                      InkWell(child:Text('البدء مرة اخري',textAlign: TextAlign.center), onTap: (){},),
                  ]),
                 
                ],
              ),
            ),
   ],)
   );
}


Widget tableWidget({title,subtitle,}){
  return Container(
    height: 130,
    child:Card(
    child:ListTile(title: Text(title),
    subtitle: Container(margin: EdgeInsets.only(right: 50),
    child:Row(children: [
      Text(subtitle),
      SizedBox(width:10),
      Container(height: 60,
         decoration: BoxDecoration(
                        color: PrimaryLightColor,
                       border: Border.all(width: 2,color: Colors.green,style: BorderStyle.solid)
                      ),)
    ],)),)
  ));
  }
Widget examAssignmentsWidget({title,subtitleLable,subtitleValue,dateStart,dateEnd}){
  return Card(
    elevation: 0,
   margin: EdgeInsets.only(left:15,right:15,bottom: 5,top:5),
   child:Column(
       mainAxisAlignment: MainAxisAlignment.start,
  crossAxisAlignment: CrossAxisAlignment.start,
     children: [
   Container(
     padding: EdgeInsets.all(10),
     child:Text(title,style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold)),),
      Container(
              margin: EdgeInsets.all(10),
              child: Table(
                // border: TableBorder.all(width: 0,color: PrimaryColor),
                children: [
                  TableRow( children: [
                      Text('المادة ',textAlign: TextAlign.start,),
                      Text('رياضيات',textAlign: TextAlign.start,style: TextStyle(color: Colors.grey),)
                  ]),

                   TableRow( children: [
                      Text('تاريخ البدء ',textAlign: TextAlign.start,),
                      Text('10-2-2020 10:2',textAlign: TextAlign.start,style: TextStyle(color: Colors.grey))
                  ]),

                   TableRow( children: [
                      Text('تاريخ الايقاف ',textAlign: TextAlign.start,),
                      Text('20-10-2020 10:10',textAlign: TextAlign.start,style: TextStyle(color: Colors.grey))
                  ]),
                 
                ],
              ),
            ),

      Container(
              margin: EdgeInsets.all(10),
              child: Table(
                
                border: TableBorder.all(width: 0,color: PrimaryColor),
                children: [
                  TableRow( children: [
                     
                     InkWell(child: Text('درجات التسليم ',textAlign: TextAlign.center,),
                     onTap: (){},),
                     
                  ]),
                 
                ],
              ),
            ),
   ],)
   );
}




Widget virtualClassRoomWidget({title,subtitleLable,lableButtom}){
  return Card(
    elevation: 0,
   margin: EdgeInsets.only(left:15,right:15,bottom: 5,top:5),
   child:Column(
       mainAxisAlignment: MainAxisAlignment.start,
  crossAxisAlignment: CrossAxisAlignment.start,
     children: [
   Container(
     padding: EdgeInsets.all(10),
     child:Text(title,style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold)),),
      Container(
              margin: EdgeInsets.all(10),
              child: Table(
                // border: TableBorder.all(width: 0,color: PrimaryColor),
                children: [
                  TableRow( children: [
                      Text('المادة ',textAlign: TextAlign.start,),
                      Text('رياضيات',textAlign: TextAlign.start,style: TextStyle(color: Colors.grey),)
                  ]),

                   TableRow( children: [
                      Text('الوقت  ',textAlign: TextAlign.start,),
                      Text('10-2-2020 10:2',textAlign: TextAlign.start,style: TextStyle(color: Colors.grey))
                  ]),

                   TableRow( children: [
                      Text('المدة الزمنية  ',textAlign: TextAlign.start,),
                      Text('٥٠ دقيقة',textAlign: TextAlign.start,style: TextStyle(color: Colors.grey))
                  ]),
                 
                ],
              ),
            ),

      Container(
              margin: EdgeInsets.all(10),
              child: Table(
                
                border: TableBorder.all(width: 0,color: PrimaryColor),
                children: [
                  TableRow( children: [
                     
                     InkWell(child: Text(lableButtom,textAlign: TextAlign.center,),
                     onTap: (){},),
                     
                  ]),
                 
                ],
              ),
            ),
   ],)
   );
}