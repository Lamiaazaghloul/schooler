import 'package:flutter/material.dart';
import 'package:schooler/uitils/const_color.dart';


 //===TextField =============================================

Widget  widgetTextField({
        String  hintText  ,
        String labelText ,
        TextEditingController controller ,
        double horizontal :0,
        double vertical:0,
        TextInputType  textInput,
        double  radius,
        TextInputAction textAction ,
       
        bool obscureText : false,
        }) {
        return new Container(
        padding: EdgeInsets.symmetric(horizontal: horizontal ,vertical: vertical ),
        // decoration: new BoxDecoration(
        //   //  color: Colors.grey[100]
        // ),
        child: TextField(
        
        keyboardType: textInput,
        controller:  controller,
        // maxLines: maxLines,
        textInputAction: textAction,
        
        
        obscureText: obscureText,
      //  decoration: InputDecoration(
      //     enabledBorder: OutlineInputBorder(
      //             borderSide: BorderSide(color: Colors.grey[200], width: 1.0),
      //     ),
 decoration: new InputDecoration(
      border: new OutlineInputBorder(
        borderRadius:BorderRadius.circular(radius),
       
        borderSide: BorderSide(
                width: 0, 
                style: BorderStyle.none,
            ),
       
      ),

        hintText: hintText,
        fillColor: AccentColor,
        filled: true,
        enabled: true,
        contentPadding: EdgeInsets.all(12.0),
        labelText: labelText,
        labelStyle: TextStyle(color: ErrorColor,fontSize: 14),
        hintStyle: TextStyle(fontSize: 14)
        ),
        ),
        );
        }
