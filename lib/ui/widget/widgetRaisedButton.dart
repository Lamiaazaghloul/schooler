import 'package:flutter/material.dart';

Widget raisedButton({String labelText,Function ontap,Color buttonColor}){
  return Padding(
  padding: EdgeInsets.only(left: 5.0, right: 5.0),
  child: RaisedButton(
    textColor: Colors.white,
    color: buttonColor,
    child:Center(child:Text(labelText,style: TextStyle(fontSize: 16),)),
    onPressed: () {ontap();},
    shape: new RoundedRectangleBorder(
      borderRadius: new BorderRadius.circular(30.0),
    ),
  ),
);
}