import 'package:flutter/material.dart';
import 'package:schooler/ui/widget/widgetRaisedButton.dart';
import 'package:schooler/uitils/change_route.dart';
import 'package:schooler/uitils/const_color.dart';

import 'login.dart';

class Interodaction extends StatefulWidget {
  @override
  _InterodactionState createState() => _InterodactionState();
}

class _InterodactionState extends State<Interodaction> {
  String interoText =
      'سكولر لقاء التحول الرقمي والتعليم عن بعد للمنشات التعليمية حيث تجمع المنصة جميع شون منشأتك التعليمية عن  منصة إلكترونية واحدة مثل الشئون التعليمية والشئون الإدارية والشئون المالية ويسهل التطبيق علي الاباء متابعة ابنائهم بكل سهولة خلال رحلتهم التعليمية ';
  String selectText = 'لتقدم في المدرسة أو الدخول في سكولر اختر اسم المدرسة ';
  @override
  Widget build(BuildContext context) {
    //  var height=MediaQuery.of(context).size.height;
    return Scaffold(
        backgroundColor: PrimaryLightColor,
        body: Container(
            padding: EdgeInsets.all(20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                // Expanded(
                // child:
                Container(
                    color: PrimaryLightColor,
                    child: Image.asset(
                      'assets/logo.png',
                      height: 150,
                      width: 260,
                    )
                    // ),
                    ),
                // Expanded(
                // child:
                Container(
                  color: PrimaryLightColor,
                  child: Text(
                    interoText,
                    textAlign: TextAlign.center,
                  ),
                ),
                // ),
                SizedBox(height: 20),
                // Expanded(
                // child:
                Container(
                  color: PrimaryLightColor,
                  child: Text(
                    selectText,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                // ),
                SizedBox(
                  height: 15,
                ),
                // Expanded(
                // child:
                Container(
                  color: PrimaryLightColor,
                  // height: 50,
                  child: Container(
                      margin: EdgeInsets.only(left: 15, right: 15),
                      padding:
                          EdgeInsets.only(left: 5, right: 5, top: 5, bottom: 5),
                      decoration: BoxDecoration(
                        color: AccentColor,
                        borderRadius: BorderRadius.circular(25),
                      ),
                      child: new DropdownButton<String>(
                        isExpanded: true,
                        underline: Container(
                          height: 0,
                        ),
                        hint:Center(child:Text('---تحديد المدرسة---')),
                        items: <String>[
                          'المدرسة الاهلية الخاصة',
                          'المدرسة الاهلية',
                          'المدرسة الاهلية الخاصة',
                          'المدرسة الاهلية'
                        ].map((String value) {
                          return new DropdownMenuItem<String>(
                            value: value,
                            child: new Text(value),
                          );
                        }).toList(),
                        onChanged: (_) {},
                      )),
                ),
                // ),
                SizedBox(height: 20),
                // Expanded(
                // child:
                Container(
                  width: 170,
                  height: 50,
                    child: raisedButton(
                        buttonColor: PrimaryDarkColor,
                        ontap: () {
                          changeScreenPush(context,Login());
                        },
                        labelText: 'دخول')),

                // ),
              ],
            )));
  }
}
