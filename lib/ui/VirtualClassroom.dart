import 'package:flutter/material.dart';
import 'package:schooler/ui/widget/layoutWidget.dart';
import 'package:schooler/ui/widget/listAppwidget.dart';
import 'package:schooler/uitils/const_color.dart';

class VirtualClassroomPage extends StatefulWidget {
  @override
  _VirtualClassroomPageState createState() => _VirtualClassroomPageState();
}
 
class _VirtualClassroomPageState extends State<VirtualClassroomPage> {
    List tabs=[' القادمة ','تم اجتيازها '];
     int _selectedIndex = 0;
    _onSelected(int index) {
        setState(() => _selectedIndex = index);
        print(index);
    }
  @override
  Widget build(BuildContext context) {
     return Scaffold(
      appBar: appBarWidget(title: " الغرفة الصفية الافتراضية ", context: context),
      body:Column(children: [
        
        Container(
          margin: EdgeInsets.symmetric(vertical: 20.0),
          height: 40.0,
          child: ListView.builder(
             scrollDirection: Axis.horizontal,
             shrinkWrap: false,
      itemCount: tabs.length,
      itemBuilder: (context, index) {
 return InkWell(
     onTap: () => _onSelected(index),
     child:Container(
     decoration: BoxDecoration(
     color: _selectedIndex != null && _selectedIndex == index
                ? PrimaryColor
                : ErrorColor,
          borderRadius: BorderRadius.circular(50),
     ),
     padding: EdgeInsets.only(left: 15,right: 15,top: 4),
       margin: EdgeInsets.only(left: 5,right: 5),
     child:Text(tabs[index],style: TextStyle(color: AccentColor),textAlign: TextAlign.center,),
  ));
            }
)),
        Expanded(
          child: ListView.builder(
        itemCount: 8,
        itemBuilder: (context, index) {
          return _selectedIndex != null && _selectedIndex == index?virtualClassRoomWidget(
              title: "الاسلوب اللغوي",
             lableButtom: 'بدء'
              ):virtualClassRoomWidget(
              title: "الخصائص والتغيرات الكيميائية",
              lableButtom: 'مسشاهدة'
             
              );
        },
      )
      )]),
    );
  }
}
