import 'package:flutter/material.dart';
import 'package:schooler/ui/widget/layoutWidget.dart';

class AutomatedCallMapPage extends StatefulWidget {
  @override
  _AutomatedCallMapPageState createState() => _AutomatedCallMapPageState();
}

class _AutomatedCallMapPageState extends State<AutomatedCallMapPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: appBarWidget(title: "النداء الألي", context: context),
        body: ListView(
          children: [
            Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/Map.png"),
                    fit: BoxFit.cover,
                  ),
                ),
                child: Image.asset("assets/Map.png")),
          ],
        )
        // floatingActionButton: ,
        );
  }
}
